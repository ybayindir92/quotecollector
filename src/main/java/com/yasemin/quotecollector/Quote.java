package com.yasemin.quotecollector;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Quote {

    private String quote;
    private String author;

    @JsonProperty("cat")
    private String category;

    public String getQuote() {
        return quote;
    }

    public void setQuote(final String quote) {
        this.quote = quote;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return quote + " -" + author;
    }
}
