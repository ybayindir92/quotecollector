package com.yasemin.quotecollector;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@SpringBootTest
@RunWith(SpringRunner.class)
public class QuoteServiceIntegrationTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private QuoteService quoteService;

    @Test
    public void shouldReturnNullWhenExceptionThrown() {
        // given
        final String url = "https://talaikis.com/api/quotes/random/";
        given(restTemplate.getForObject(url, Quote.class)).willThrow(new RestClientException("Rest exception"));

        // when
        final Quote actual = quoteService.requestQuote();

        // then
        assertNull(actual);
    }

    @Test
    public void shouldPopulateCorrectUrl() {
        // given
        final String expectedUrl = "https://talaikis.com/api/quotes/random";

        // when
        quoteService.requestQuote();

        // then
        verify(restTemplate).getForObject(expectedUrl, Quote.class);
    }

}