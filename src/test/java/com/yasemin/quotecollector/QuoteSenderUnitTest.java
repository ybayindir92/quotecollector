package com.yasemin.quotecollector;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import static com.yasemin.quotecollector.RabbitConfiguration.QUOTE_BINDING;
import static com.yasemin.quotecollector.RabbitConfiguration.QUOTE_EXCHANGE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class QuoteSenderUnitTest {

    @Mock
    private QuoteService quoteService;

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private RabbitTemplate rabbitTemplate;

    @InjectMocks
    private QuoteSender quoteSender;

    @Test
    public void shouldRequestQuote() throws Exception {
        // when
        quoteSender.sendRandomQuote();

        // then
        verify(quoteService).requestQuote();
    }

    @Test
    public void shouldConvertQuoteToJson() throws Exception {
        // given
        Quote quote = new Quote();
        given(quoteService.requestQuote()).willReturn(quote);

        // when
        quoteSender.sendRandomQuote();

        // then
        verify(objectMapper).writeValueAsString(quote);
    }

    @Test
    public void shouldSendMessage() throws Exception {
        // given
        Quote quote = new Quote();
        String jsonMessage = "{message}";
        given(quoteService.requestQuote()).willReturn(quote);
        given(objectMapper.writeValueAsString(any())).willReturn(jsonMessage);

        // when
        quoteSender.sendRandomQuote();

        // then
        verify(rabbitTemplate).convertAndSend(QUOTE_EXCHANGE, QUOTE_BINDING, jsonMessage);
    }

    @Test
    public void shouldNotSendNullQuote() throws Exception {
        // given
        given(quoteService.requestQuote()).willReturn(null);

        // when
        quoteSender.sendRandomQuote();

        // then
        verify(rabbitTemplate, never()).convertAndSend(anyString(), anyString(), anyString());
    }

}