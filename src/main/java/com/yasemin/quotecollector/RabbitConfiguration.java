package com.yasemin.quotecollector;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfiguration {

    static final String QUOTE_EXCHANGE = "quote-exchange";
    static final String QUOTE_BINDING = "quote.binding";
    private static final String QUOTE_QUEUE = "quote-queue";

    @Bean
    Queue quoteQueue() {
        return new Queue(QUOTE_QUEUE);
    }

    @Bean
    DirectExchange quoteDirectExchange() {
        return new DirectExchange(QUOTE_EXCHANGE);
    }

    @Bean
    Binding quoteBinding() {
        return BindingBuilder.bind(quoteQueue()).to(quoteDirectExchange()).with(QUOTE_BINDING);
    }

}
