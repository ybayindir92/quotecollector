package com.yasemin.quotecollector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class QuoteService {

    private static final Logger log = LoggerFactory.getLogger(QuoteService.class);
    private final RestTemplate restTemplate;

    @Value("${quoteUrl}")
    private String quoteUrl;

    public QuoteService(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    Quote requestQuote() {
        try {
            return restTemplate.getForObject(quoteUrl, Quote.class);
        } catch (RestClientException e) {
            log.warn("Problem occurred while getting quote: " + e.getMessage());
            return null;
        }
    }

}
