package com.yasemin.quotecollector;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import static com.yasemin.quotecollector.RabbitConfiguration.QUOTE_BINDING;
import static com.yasemin.quotecollector.RabbitConfiguration.QUOTE_EXCHANGE;

@Service
public class QuoteSender {

    private static final Logger log = LoggerFactory.getLogger(QuoteService.class);
    private final QuoteService quoteService;
    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    @Autowired
    public QuoteSender(final QuoteService quoteService, final RabbitTemplate rabbitTemplate, final ObjectMapper objectMapper) {
        this.quoteService = quoteService;
        this.rabbitTemplate = rabbitTemplate;
        this.objectMapper = objectMapper;
    }

    @Scheduled(fixedRate = 60000)
    public void sendRandomQuote() throws JsonProcessingException {
        final Quote quote = quoteService.requestQuote();
        if (quote != null) {
            log.info("Message is sending...");
            rabbitTemplate.convertAndSend(QUOTE_EXCHANGE, QUOTE_BINDING, objectMapper.writeValueAsString(quote));
        }
    }

}
